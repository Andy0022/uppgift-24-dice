package com.example.umyhfilian.uppgift24actiondice;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by umyhblomti on 2015-11-11.
 */
public class Downloader {

    public static Bitmap downloadBitmap(String strURL){
        InputStream stream = connect(strURL);
        Bitmap btmp = BitmapFactory.decodeStream(stream);
        return btmp;
    }

    public static String downloadString(String strURL){
        InputStream stream = connect(strURL);
        Charset charset = Charset.forName("ISO-8859-1");
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
        StringBuilder builer = new StringBuilder();
        String line = "";
        try {
            while ((line = reader.readLine()) != null) {
                builer.append(line);
            }
            reader.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return builer.toString();
    }

    private static InputStream connect(String strUrl){
        InputStream stream = null;
        try {
            URL url = new URL(strUrl);
            if (strUrl.substring(0, 5).equals("https")) {
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                stream = connection.getInputStream();
            } else {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                stream = connection.getInputStream();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return stream;
    }
}
