package com.example.umyhfilian.uppgift24actiondice;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.seismic.ShakeDetector;

import org.json.JSONObject;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements ShakeDetector.Listener{

    //number of sides of the dice
    int typeOfDice = 6;
    int result = 0;
    String urlEnding = "";
    MediaPlayer rollMP;
    ImageLoader imgLdr;
    MyAsyncStringDownload stringDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupSeismic();
        setupSoundFX();
        setupImgLdr();
    }

    public void onClickRoll(View view){
        rollMP.start();
        result = rollDice();
        stringDownload = new MyAsyncStringDownload();
        stringDownload.execute(Integer.toString(result));
    }

    private int rollDice() {
        Random r = new Random();
        int Low = 1;
        int High = typeOfDice;
        int result = r.nextInt(High+1 - Low) + Low;
        return result;
    }

    public void createDiceFrag(String picUrl){
        Log.i("MINTAG", picUrl);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DiceResultFragment fragment = new DiceResultFragment();

        if(fragmentManager.findFragmentByTag("TAG") != null){
            //Log.i("TAG","FRAGMENTET ÄR ADDAT");
            fragmentTransaction.show(fragment);
            TextView textViewNounEnding = (TextView)findViewById(R.id.diceResultTextViewNounEnding);
            textViewNounEnding.setText(urlEnding.substring(2));
            ImageView imageView = (ImageView)findViewById(R.id.diceResultImageView);
            imageView.setImageDrawable(null);
            imgLdr.displayImage(picUrl, imageView);
        }
        else{
            //Log.i("TAG","FRAGMENTET ÄR EJ ADDAT");
            fragmentTransaction.add(R.id.fragmentContainer, fragment, "TAG");
            fragmentTransaction.commit();
        }
    }

    public void onClickChangeDice(View view){
        TextView textViewDiceType = (TextView)findViewById(R.id.textViewDiceType);
        typeOfDice = typeOfDice + 1;
        textViewDiceType.setText("1 - " + Integer.toString(typeOfDice) + " Dice");
    }

   public void onClickResetDice(View view){
        TextView textViewDiceType = (TextView)findViewById(R.id.textViewDiceType);
        typeOfDice = 2;
        textViewDiceType.setText("1 - " + Integer.toString(typeOfDice) + " Dice");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupSeismic() {
        SensorManager senMan = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector shakeDetec = new ShakeDetector(this);
        shakeDetec.start(senMan);
    }

    @Override
    public void hearShake() {
        rollMP.start();
        result = rollDice();
        stringDownload = new MyAsyncStringDownload();
        stringDownload.execute(Integer.toString(result));
    }

    private void setupSoundFX() {
        rollMP = MediaPlayer.create(this, R.raw.roll);
    }

    private void setupImgLdr() {
        imgLdr = ImageLoader.getInstance();
        imgLdr.init(ImageLoaderConfiguration.createDefault(this));
    }

    private class MyAsyncStringDownload extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            urlEnding = params[0] + "+" + Nouns.randomNoun() + "s";
            String httpToGooglePicUrls = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + urlEnding;
            return Downloader.downloadString(httpToGooglePicUrls);
        }

        @Override
        protected void onPostExecute(String jsonString) {
            String bitmpsURLString = extractURLFromJSONString(jsonString);
            createDiceFrag(bitmpsURLString);
        }

        public String extractURLFromJSONString(String jsonString){
            try {
                JSONObject json = new JSONObject(jsonString);
                String imageUrl = json.getJSONObject("responseData").getJSONArray("results").getJSONObject(0).getString("url");
                return imageUrl;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }
}
